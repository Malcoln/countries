package com.udemy.countries

import com.udemy.countries.model.CountriesApi
import com.udemy.countries.model.Country
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

object ApiModule {

    //private val BASE_URL = "https://raw.githubusercontent.com"
    private val api : CountriesApi
    private val api2 : TesteApi

        @JvmStatic var BASE_URL = "https://raw.githubusercontent.com"
        @JvmStatic var builder: Retrofit.Builder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(BASE_URL)
        @JvmStatic
        fun changeApiBaseUrl(newApiBaseUrl:String):String{
            BASE_URL = newApiBaseUrl
            builder = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BASE_URL)

            return BASE_URL

        }

    init {

        api = Retrofit.Builder()
            .baseUrl(changeApiBaseUrl("https://raw.githubusercontent.com"))
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(CountriesApi::class.java)

        api2 = Retrofit.Builder()
            .baseUrl(changeApiBaseUrl("https://raw.githubusercontent.com"))
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(TesteApi::class.java)
    }

    fun getNewUrl():Single<List<Country>>{
        return api.getCountries()
    }

    fun getAuth():Single<List<Country>>{
        return  api2.getCountries()
    }


    interface TesteApi {
        @GET("DevTides/countries/master/countriesV2.json")
        fun getCountries(): Single<List<Country>>
    }

}