package com.udemy.countries.model

import io.reactivex.Single
import retrofit2.http.GET

interface ExampleApi {
    @GET("DevTides/countries/master/countriesV2.json")
    fun getExamplesUrlDynamicsCountries() : Single<List<Country>>
}