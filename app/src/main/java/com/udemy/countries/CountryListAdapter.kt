package com.udemy.countries

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.udemy.countries.model.Country
import kotlinx.android.synthetic.main.item_country.view.*

class CountryListAdapter(var countries: ArrayList<Country>) :
    RecyclerView.Adapter<CountryListAdapter.CountryVH>() {

    fun updateCountries(newCountry: List<Country>) {
        countries.clear()
        countries.addAll(newCountry)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CountryVH(
        LayoutInflater.from(parent.context).inflate(R.layout.item_country, parent, false)
    )

    override fun getItemCount() = countries.size

    override fun onBindViewHolder(holder: CountryVH, position: Int) {
        holder.bind(countries[position])
    }

    class CountryVH(view: View) : RecyclerView.ViewHolder(view) {

        private val countryName = view.name
        fun bind(country: Country) {
            countryName.text = country.countryName
        }
    }
}

