package com.udemy.countries.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.udemy.countries.ApiModule
import com.udemy.countries.model.CountriesService
import com.udemy.countries.model.Country
import com.udemy.countries.model.ExampleService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class ListViewModel : ViewModel() {
    // 3 variables herdam de LiveData, LiveData é uma variavel que pode se assinada (subscribe)
    // e acessa a classe Data em tempo real, com atualizações todas as variaveis sao atualizadas

    private val countriesService = CountriesService()
    private val disposable = CompositeDisposable()

    private val exampleService = ExampleService()

    val countries = MutableLiveData<List<Country>>()
    // Todos que assinarem a variavel countries serao notificados quando a variavel for modificada
    val countryLoadError = MutableLiveData<Boolean>() // vai transmitir true or false
    val loading =
        MutableLiveData<Boolean>() // vai transmitir o estado de carregamento se tre or false

    fun refresh() {
        //fetchCountries()
        fetchExampleUrlDynamics()
    }

    private fun fetchCountries() {
        loading.value = true
        disposable.add(
            countriesService.getCountries()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<List<Country>>() {
                    override fun onSuccess(value: List<Country>?) {
                        countries.value = value
                        countryLoadError.value = false
                        loading.value = false
                    }
                    override fun onError(e: Throwable?) {
                        countryLoadError.value = true
                        loading.value = false
                    }
                })
        )
    }

    private fun fetchExampleUrlDynamics(){

        loading.value = true
        disposable.add(
            ApiModule.getAuth()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<List<Country>>(){
                    override fun onSuccess(value: List<Country>?) {
                        countries.value = value
                        countryLoadError.value = false
                        loading.value = false
                    }

                    override fun onError(e: Throwable?) {
                        countryLoadError.value = true
                        loading.value = false
                    }

                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}